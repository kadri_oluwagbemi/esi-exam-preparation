package com.example.StrelsauTaxi.StrelsauTaxi.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue

    Long id;

    String first_name;

    String last_name;

    String address;

    String telephone_no;
}
