package com.example.StrelsauTaxi.StrelsauTaxi.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class TripRequest {
    @Id
    @GeneratedValue
    Long id;

    String start_address;

    String end_address;

//    @ManyToOne
//    User user;

    int trip_duration;
}
