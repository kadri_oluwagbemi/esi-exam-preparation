package com.example.StrelsauTaxi.StrelsauTaxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrelsauTaxiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrelsauTaxiApplication.class, args);
	}
}
