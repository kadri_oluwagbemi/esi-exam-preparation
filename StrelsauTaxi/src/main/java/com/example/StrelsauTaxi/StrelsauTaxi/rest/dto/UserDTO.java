package com.example.StrelsauTaxi.StrelsauTaxi.rest.dto;

import lombok.Data;

@Data
public class UserDTO {
    Long id;

    String firstname;

    String lastname;

    String address;

    int telephoneNo;
}
