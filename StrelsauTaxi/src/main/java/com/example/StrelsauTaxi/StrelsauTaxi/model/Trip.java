package com.example.StrelsauTaxi.StrelsauTaxi.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class Trip {
    @Id
    @GeneratedValue

    Long id;
//
//    TripRequest trip_request;

    TripStatus status;

    BigDecimal price;

//    @OneToOne
//    Drivers driver;

}
