package repository;

import com.example.StrelsauTaxi.StrelsauTaxi.model.TripHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripHistoryRepository extends JpaRepository<TripHistory, Long> {
}
