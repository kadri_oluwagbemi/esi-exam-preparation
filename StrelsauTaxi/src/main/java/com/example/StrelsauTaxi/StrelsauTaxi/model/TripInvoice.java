package com.example.StrelsauTaxi.StrelsauTaxi.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class TripInvoice {
    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    Trip trip;
}
