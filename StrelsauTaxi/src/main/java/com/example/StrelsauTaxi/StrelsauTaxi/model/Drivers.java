package com.example.StrelsauTaxi.StrelsauTaxi.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Drivers {
    @Id
    @GeneratedValue

    Long id;

    String firstname;

    String lastname;

    @Enumerated(EnumType.STRING)
    DriverStatus DriverStatus;
}
