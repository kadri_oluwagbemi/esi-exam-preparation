package repository;

import com.example.StrelsauTaxi.StrelsauTaxi.model.Drivers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverRepository extends JpaRepository <Drivers, Long>{
}
