package com.example.StrelsauTaxi.StrelsauTaxi.model;

public enum TripStatus {COMPLETED, NOTCOMPLETED, WAITING_FOR_DRIVER}
