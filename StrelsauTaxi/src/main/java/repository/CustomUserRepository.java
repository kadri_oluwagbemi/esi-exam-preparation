package repository;

import com.example.StrelsauTaxi.StrelsauTaxi.model.User;

import java.util.List;

public interface CustomUserRepository {
    List<User> getUSers(String first_name);

}
